import pytest as pt
import tamagotci


def test_status_generator(min_value=0, current_value=100, max_value=100):
    d = {'min': min_value,
         'current': current_value,
         'max': max_value}
    assert tamagotci.generate_status_bar() == d
